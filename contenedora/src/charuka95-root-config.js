import { registerApplication, start } from "single-spa";

registerApplication({
  name: "@single-spa/welcome",
  app: () =>
    System.import(
      "https://unpkg.com/single-spa-welcome/dist/single-spa-welcome.js"
    ),
  activeWhen: ["/"],
});

registerApplication({
  name: "@charuka95/spa-demo-header",
  app: () => System.import("@charuka95/spa-demo-header"),
  activeWhen: ["/"]
});

registerApplication({
  name: "@charuka95/spa-demo-welcome",
  app: () => System.import("@charuka95/spa-demo-welcome"),
  activeWhen: ["/"]
});

registerApplication({
  name: "@charuka95/spa-demo-test",
  app: () => System.import("@charuka95/spa-demo-test"),
  activeWhen: ["/"]
});

// registerApplication({
//   name: "@charuka95/navbar",
//   app: () => System.import("@charuka95/navbar"),
//   activeWhen: ["/"]
// });

start({
  urlRerouteOnly: true,
});
